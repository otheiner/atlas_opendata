# ATLAS OpenData - working example

This repository contains two exercises with ATLAS Open Data. Running the code requires having access to the ATLAS OpenData (connection to the Internet), installed ROOT and python. In case you want to avoid installation of ROOT you can run the code using Binder (https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/master) which is a web service using Docker images that contain all necessary software. It is very straightforward. One can upload their jupyter notebook and run the code. There are examples of two physic cases:

1. Reconstruction of W boson transverse mass from W->enu and W->munu decays. This contains code in jupyter notebook and also standalone python script. 

![](Wlnu_stacked.png)

2. Reconstruction of Z boson mass from Z->ll channel

![](Zll.png)
