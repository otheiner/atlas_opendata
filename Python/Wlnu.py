import ROOT
import math
#%jsroot on

print("Opening file")
f = ROOT.TFile.Open("./Data/data_A.1lep.root ") ## 13 TeV sample

hist_stack = ROOT.THStack("stacked","Transverse mass of the W boson; mass [GeV]; events")
hist_enu = ROOT.TH1F("enu","Transverse mass of the W boson; mass [GeV]; events",100,-0.5,200.5)
hist_enu.SetFillColor(2)
hist_munu = ROOT.TH1F("munu","Transverse mass of the W boson; mass [GeV]; events",100,-0.5,200.5)
hist_munu.SetFillColor(3)
hist_taunu = ROOT.TH1F("taunu","Transverse mass of the W boson; mass [GeV]; events",100,-0.5,200.5)
hist_taunu.SetFillColor(4)
hist_ptcone30 = ROOT.TH1F("ptcone","P_T cone 30; ptcone30; events",100,-0.1,0.5)
hist_taunu.SetFillColor(2)
tree = f.Get("mini")
event_count = tree.GetEntries()
print("Entries: ", event_count)
counter = 0
resets = 0
Wplus = 0
Wminus = 0


for event in tree:
    counter += 1
    if round(counter/event_count * resets, 2) == 0.2: #Only for testing
        break
    if counter == round(event_count * 0.01):
        resets += 1
        print("Progress: ", round(counter/event_count * resets, 2))
        counter = 0
    if tree.lep_n > 1:
        print("lepton multiplicity greater than 1: ", tree.lep_n)
    #Event selection
    hist_ptcone30.Fill(tree.lep_ptcone30[0])
    if tree.trigE or tree.trigM:
        if tree.lep_pt[0] > 35000: #Lepton p_t > 35 GeV, lep_pt in MeV
            if tree.met_et > 30000: #Missing E_T > 30 GeV, lep_pt in MeV
                M_WT = math.sqrt(2 * tree.lep_pt[0] * tree.met_et * (1 - math.cos(tree.lep_phi[0] - tree.met_phi)))
                #print(M_WT)
                if M_WT > 0 : #Reconstructed transverse momentum is above 60 GeV
                    if tree.lep_type[0] == 11 or tree.lep_type[0] == -11:
                        hist_enu.Fill(M_WT / 1000)
                        if tree.lep_charge[0] == -1:
                            Wminus += 1
                        if tree.lep_charge[0] == 1:
                            Wplus += 1
                    if tree.lep_type[0] == 13 or tree.lep_type[0] == -13:
                        hist_munu.Fill(M_WT / 1000)
                        if tree.lep_charge[0] == -1:
                            Wminus += 1
                        if tree.lep_charge[0] == 1:
                            Wplus += 1
                    if tree.lep_type[0] == 15 or tree.lep_type[0] == -15:
                        hist_taunu.Fill(M_WT / 1000)

canvas1 = ROOT.TCanvas("Canvas1","c1",800,600)
canvas1.cd()
hist_stack.Add(hist_enu)
hist_stack.Add(hist_munu)
hist_stack.Add(hist_taunu)
hist_stack.Draw("")
canvas1.Draw("hist")
legend = ROOT.TLegend(1.0,1.0,1.0,1.0)
legend.AddEntry(hist_munu, "W->#mu+#nu")
legend.AddEntry(hist_enu, "W->e+#nu")
legend.Draw()
canvas1.Print("Wlnu_stacked.png")

canvas2 = ROOT.TCanvas("Canvas2","c2",800,600)
canvas2.cd()
hist_munu.SetFillColorAlpha(3, 0.5)
hist_munu.Draw("SAME")
hist_enu.SetFillColorAlpha(2, 0.5)
hist_enu.Draw("SAME")
legend2 = ROOT.TLegend(0.65,0.45,0.81,0.62)
legend2.AddEntry(hist_munu, "W->#mu+#nu")
legend2.AddEntry(hist_enu, "W->e+#nu")
legend2.Draw()
canvas2.Draw("hist")
canvas2.Print("Wlnu_overlayed.png")

ratio = hist_enu.Integral()/hist_munu.Integral()
print("Ratio (W->e+nu)/(W->mu+nu) from data is ", "{:0.2f}".format(ratio), ", theoretical value is 1.01")
print("Wplus ", Wplus, ", Wminus ", Wminus)
print("R+- =", Wplus / Wminus, ", value measured by ATLAS is 1.22 (https://atlas.physicsmasterclasses.org/en/wpath_auswertung2.htm)")
