import ROOT
import math

print("Opening file")
#Z reconstruction
#f = ROOT.TFile.Open("https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/2lep/Data/data_A.2lep.root") ## 13 TeV sample
f = ROOT.TFile.Open("./Data/data_A.2lep.root") ## 13 TeV sample
canvas4 = ROOT.TCanvas("Canvas4","c4",800,600)
hist_Z_ll = ROOT.TH1F("Zll", "Mass of the Z boson; mass [GeV]; events",100,65.5,115.5)

tree = f.Get("mini")
event_count = tree.GetEntries()
print("Entries: ", event_count)
counter = 0
resets = 0

for event in tree:
    counter += 1
    if round(counter/event_count * resets, 2) == 1.1: #Only for testing
        break
    if counter == round(event_count * 0.01):
        resets += 1
        print("Progress: ", round(counter/event_count * resets, 2))
        counter = 0
    if tree.trigE or tree.trigM:
        if tree.lep_n == 2:
            if tree.lep_type[0] == tree.lep_type[1]:
                if not tree.lep_charge[0] == tree.lep_charge[1]:
                    p1 = ROOT.TLorentzVector()
                    p1.SetPtEtaPhiE(tree.lep_pt[0], tree.lep_eta[0], tree.lep_phi[0], tree.lep_E[0])
                    p2 = ROOT.TLorentzVector()
                    p2.SetPtEtaPhiE(tree.lep_pt[1], tree.lep_eta[1], tree.lep_phi[1], tree.lep_E[1])
                    mass = math.sqrt((p1 + p2)*(p1 + p2))/1000 #conversion to GeV
                    if mass >= 66 and mass <= 116:
                        hist_Z_ll.Fill(mass)

hist_Z_ll.SetFillColor(2)
canvas4.cd()
hist_Z_ll.Draw("")                    
canvas4.Draw("hist")
canvas4.Print("Zll.png")
